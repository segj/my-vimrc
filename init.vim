"Written for NVIM v0.6.0


"REMAPS
"======

"no cursor jumping after visual yank
"vnoremap y myy`y
"vnoremap Y myY`y
"disabled because it disables visual yanking to a register

"easier redo
:nnoremap U <C-r>

"easier find repeat
:nnoremap ; ,
:nnoremap , ;
:vnoremap ; ,
:vnoremap , ;

"easy commands with enter
:nnoremap <CR> :
:vnoremap <CR> :

"linux version of above
:nnoremap  :
:vnoremap  :

"standard file saving
:nnoremap <C-s> :w<CR>

"easy newlines
:nnoremap ö ok
:nnoremap Ö Oj

"split line (reverse of J for join)
:nnoremap K i<CR><Esc>k$

"move ^ to a non-dead key
:nnoremap å ^

"move ~ to a non-dead key
:nnoremap Å ~

"toggle word wrap
:nnoremap <F7> :set wrap!<CR>

"yank file contents
:nnoremap <F8> :%y<CR>

"easy window hopping
:nnoremap <C-j> <C-w>j
:nnoremap <C-k> <C-w>k
:nnoremap <C-h> <C-w>h
:nnoremap <C-l> <C-w>l

"easy buffers
:nnoremap gb :ls<CR>:b

"easy switch to alternate file on nordic layout
:nnoremap ga :b#<CR>

"switch between C++ header and implementation files
:nnoremap gh :e %:p:s,.hpp$,.X123X,:s,.cpp$,.hpp,:s,.X123X$,.cpp,<CR>

"switch between C header and implementation files
:nnoremap gc :e %:p:s,.h$,.X123X,:s,.c$,.h,:s,.X123X$,.c,<CR>

"center cursorline after search jump
:nnoremap n nzz
:nnoremap N Nzz

"backspace removes search highlights
:nnoremap <BS> :noh<CR>


"SETTINGS
"========

"windows-specific settings
let s:is_win = has('win32') || has('win64')
if s:is_win
	"<C-z> freezes nvim in powershell
	nmap <C-z> :echo "< < ==== <C-z> D I S A B L E D ==== > >"<CR>
endif

set mouse=a

"don't change capitalization of filenames
set backupcopy=yes

set noswapfile
set undofile

"enable switching buffers with unsaved changes
set hidden

set clipboard+=unnamed

set smartindent
set tabstop=4
set shiftwidth=4

"these two were in vimrc, not sure if needed at all
set autoindent
set noexpandtab

set hlsearch
set incsearch

set ignorecase
set smartcase

"this way the annoying rendering bug in insert mode doesn't happen with Windows
"but I can still see where I am
set cursorline

"this line throws an error on linux
"set cursorlineopt=number

set number
set relativenumber
set laststatus=2

set linebreak
"set showbreak=+
set listchars+=extends:+,trail:.
set list

set sidescroll=1
set sidescrolloff=10

syntax on
filetype on


"FILETYPE SETTINGS
"=================

autocmd filetype cpp set cc=80,101
autocmd filetype cpp set nowrap

autocmd filetype hpp set cc=80,101
autocmd filetype hpp set nowrap

autocmd filetype hh set cc=80,101
autocmd filetype hh set nowrap

autocmd filetype c set cc=80,101
autocmd filetype c set nowrap

autocmd filetype h set cc=80,101
autocmd filetype h set nowrap

autocmd filetype java set cc=80,101
autocmd filetype java set nowrap

autocmd filetype python set cc=80,101
autocmd filetype python set nowrap

autocmd filetype javascript set cc=80,101
autocmd filetype javascript set nowrap

autocmd filetype html set cc=80,101

autocmd filetype css set cc=80,101

autocmd filetype gitcommit set cc=51,73
autocmd filetype gitcommit set nowrap


"COLORS
"======

hi whitespace ctermfg=darkgrey

hi colorcolumn ctermbg=darkgrey

hi cursorlinenr ctermfg=yellow
hi linenr ctermfg=grey

hi tablinesel ctermbg=blue
hi tabline ctermbg=grey
hi tabline ctermfg=black


"SCROLLBAR
"=========
"
"func! STL()
"	let stl = '%f [%{(&fenc==""?&enc:&fenc).((exists("+bomb") && &bomb)?",B":"")}%M%R%H%W] %y [%l/%L,%v] [%p%%]'
"	let barWidth = &columns - 65 " <-- wild guess
"	let barWidth = barWidth < 3 ? 3 : barWidth
"
"	if line('$') > 1
"		let progress = (line('.')-1) * (barWidth-1) / (line('$')-1)
"	else
"		let progress = barWidth/2
"	endif
"
"" line + vcol + %
"	let pad = strlen(line('$'))-strlen(line('.')) + 3 - strlen(virtcol('.')) + 3 - strlen(line('.')*100/line('$'))
"	let bar = repeat(' ',pad).' [%1*%'.barWidth.'.'.barWidth.'('
"	        \.repeat('-',progress )
"	        \.'%2*0%1*'
"	        \.repeat('-',barWidth - progress - 1).'%0*%)%<]'
"
"	return stl.bar
"endfun
"
"hi def link User1 DiffAdd
"hi def link User2 DiffDelete
"set stl=%!STL()
