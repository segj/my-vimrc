"Written for VIM 8.2


set nocompatible


"REMAPS
"======

"symmetrical yank behaviour with D and C
:nnoremap Y y$

"no cursor jumping after visual yank
vnoremap y myy`y
vnoremap Y myY`y

"better ci( etc. behaviour
:nnoremap ci( f(ci(
:nnoremap ci[ f[ci[
:nnoremap ci{ f{ci{
:nnoremap ci< f<ci<
"use ci) etc. if already in the delimeters
"ci" already behaves this way

"better ca( etc. behaviour
:nnoremap ca( f(ca(
:nnoremap ca[ f[ca[
:nnoremap ca{ f{ca{
:nnoremap ca< f<ca<

"easier redo
:nnoremap U <C-r>

"intuitive find repeat
:nnoremap ; ,
:nnoremap , ;
:vnoremap ; ,
:vnoremap , ;

"easy commands with enter
:nnoremap <CR> :
:vnoremap <CR> :

"standard file saving
:nnoremap <C-s> :w<CR>

"easy newlines
:nnoremap ö ok
:nnoremap Ö Oj

"split line (reverse of J for join)
:nnoremap K i<CR><Esc>k$

"move ^ to a non-dead key
:nnoremap å ^

"move ~ to a non-dead key
:nnoremap Å ~

"toggle word wrap
:nnoremap <F7> :set wrap!<CR>

"yank buffer
:nnoremap <F8> :%y<CR>

"easy window hopping
:nnoremap <C-j> <C-w>j
:nnoremap <C-k> <C-w>k
:nnoremap <C-h> <C-w>h
:nnoremap <C-l> <C-w>l

"easy buffers
:nnoremap gb :ls<CR>:b

"easy switch to alternate file on nordic layout
:nnoremap ga :b#<CR>

"switch between C++ header and implementation files
:nnoremap gh :e %:p:s,.hpp$,.X123X,:s,.cpp$,.hpp,:s,.X123X$,.cpp,<CR>

"switch between C header and implementation files
:nnoremap gc :e %:p:s,.h$,.X123X,:s,.c$,.h,:s,.X123X$,.c,<CR>

"center cursorline after search jump
:nnoremap n nzz
:nnoremap N Nzz

"backspace removes search highlights
:nnoremap <BS> :noh<CR>


"SETTINGS
"========

"Windows-specific settings
let s:is_win = has('win32') || has('win64')
if s:is_win
	"<C-z> freezes nvim in powershell
	nmap <C-z> :echo "< < ==== <C-z> D I S A B L E D ==== > >"<CR>
endif

set fileencoding=utf-8
set encoding=utf-8

"apparently some old people put two spaces between sentences
set nojoinspaces

"backspace has sane functionality in insert mode
set backspace=indent,eol,start

"don't change capitalization of filenames
set backupcopy=yes

set noswapfile
set undofile
set undodir=~\vimfiles\undodir

"enable switching buffers with unsaved changes
set hidden

set clipboard+=unnamed

set belloff=all

set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set noexpandtab

set hlsearch
set incsearch

set ignorecase
set smartcase

set cursorline
set number
set relativenumber
set laststatus=2

set linebreak
"set showbreak=+
set listchars=extends:+,trail:.,tab:>\ 
set list

set sidescroll=1
set sidescrolloff=10

syntax on
filetype on


"FILETYPE SETTINGS
"=================

autocmd filetype cpp set cc=80,101
autocmd filetype cpp set nowrap

autocmd filetype hpp set cc=80,101
autocmd filetype hpp set nowrap

autocmd filetype hh set cc=80,101
autocmd filetype hh set nowrap

autocmd filetype c set cc=80,101
autocmd filetype c set nowrap

autocmd filetype h set cc=80,101
autocmd filetype h set nowrap

autocmd filetype java set cc=80,101
autocmd filetype java set nowrap

autocmd filetype python set cc=80,101
autocmd filetype python set nowrap

autocmd filetype javascript set cc=80,101
autocmd filetype javascript set nowrap

autocmd filetype html set cc=80,101

autocmd filetype css set cc=80,101

autocmd filetype gitcommit set cc=51,73
autocmd filetype gitcommit set nowrap


"COLORS
"======

hi specialkey ctermfg=darkgrey

hi cursorline ctermbg=black
hi cursorlinenr ctermbg=black
hi cursorlinenr ctermfg=yellow
hi linenr ctermfg=grey

hi colorcolumn ctermbg=darkgrey

hi tablinesel ctermbg=blue
hi tablinesel ctermfg=black
hi tabline ctermbg=grey
hi tabline ctermfg=black
hi tablinefill ctermbg=darkgrey

hi statusline ctermbg=white

"for some reason darkgrey colorcolumn doesn't show up in gitcommits
autocmd filetype gitcommit hi colorcolumn ctermbg=red


"SCROLLBAR
"=========

func! STL()
	let stl = '%f [%{(&fenc==""?&enc:&fenc).((exists("+bomb") && &bomb)?",B":"")}%M%R%H%W] %y [%l/%L,%v] [%p%%]'
	let barWidth = &columns - 65 " <-- wild guess
	let barWidth = barWidth < 3 ? 3 : barWidth

	if line('$') > 1
		let progress = (line('.')-1) * (barWidth-1) / (line('$')-1)
	else
		let progress = barWidth/2
	endif

" line + vcol + %
	let pad = strlen(line('$'))-strlen(line('.')) + 3 - strlen(virtcol('.')) + 3 - strlen(line('.')*100/line('$'))
	let bar = repeat(' ',pad).' [%1*%'.barWidth.'.'.barWidth.'('
	        \.repeat('-',progress )
	        \.'%2*0%1*'
	        \.repeat('-',barWidth - progress - 1).'%0*%)%<]'

	return stl.bar
endfun

hi def link User1 DiffAdd
hi def link User2 DiffDelete
set stl=%!STL()
